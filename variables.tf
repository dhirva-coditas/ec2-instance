variable "environment" {
  type = string
}
variable "product" {
  type = string
}
variable "use_case" {
  type = string
}
variable "disable_api_termination" {
  type = bool
}
variable "instance_type" {
  type = string
}
variable "ami" {
  type = string
}
variable "associate_public_ip_address" {
  type = bool
}
variable "subnet_id" {
  type = string
}
variable "root_block_device" {
  type = list(any)
}
variable "availability_zone" {
  type = string
}
variable "dns_zone_name" {
  type = string
}
variable "ssh_key_bucket_name" {
  type = string
}
variable "vpc_id" {
  type = string
}
variable "ingress_rules" {
  type = any
}
variable "ebs_block_device" {
  type    = list(any)
  default = []
}
variable "iam_instance_profile" {
  type    = string
  default = null
}
variable "monitoring" {
  type    = bool
  default = false
}
variable "user_data" {
  type    = string
  default = null
}
variable "remote_exec_code" {
  type    = string
  default = "whoami"
}
variable "remote_exec_using_public_ip" {
  type    = bool
  default = false
}



variable "egress_rules" {
  type = any
  default = {
    1 = {
      protocol        = "-1"
      cidr_blocks     = ["0.0.0.0/0"]
      from_port       = 0
      to_port         = 0
      security_groups = null
    }
  }
}
variable "enable_volume_tags" {
  type    = bool
  default = false
}
variable "ssh_user" {
  type    = string
  default = "ubuntu"
}
variable "private_record_ttl" {
  type    = number
  default = 300
}
variable "ebs_optimized" {
  type    = bool
  default = null
}
variable "hold_remote_exec_for_sec" {
  type    = number
  default = null
}
variable "create_eip" {
  type    = bool
  default = false
}
variable "save_private_key" {
  type    = bool
  default = false
}
variable "is_dns_zone_private" {
  type    = bool
  default = true
}
variable "is_dns_required" {
  type    = bool
  default = true
}
variable "spot_block_duration_minutes" {
  description = "The required duration for the Spot instances, in minutes. This value must be a multiple of 60 (60, 120, 180, 240, 300, or 360)"
  type        = number
  default     = 60
}
variable "create_spot_instance" {
  description = "Depicts if the instance is a spot instance"
  type        = bool
  default     = false
}
variable "use_default_tags" {
  type    = bool
  default = false
}