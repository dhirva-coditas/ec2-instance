locals {
  tags = var.use_default_tags ? {} : {
    Environment             = var.environment
    Product                 = var.product
    Use_case                = var.use_case
    Can_be_deleted          = true
    Created_using_terraform = true
  }
  name     = "${var.environment}-${var.product}-${var.use_case}-server"
  dns_name = "${var.environment}-${var.product}-${var.use_case}"
}