variable "environment" {
  type = string
}

variable "use_case" {
  type = string
}

variable "product" {
  type = string
}

variable "storage_bucket_name" {
  type = string
}

variable "save_private_key" {
  type    = bool
  default = false
}
variable "use_default_tags" {
  type    = bool
  default = false
}