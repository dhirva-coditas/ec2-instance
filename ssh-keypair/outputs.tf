output "public_key" {
  value     = tls_private_key.this.public_key_openssh
  sensitive = true
}

output "private_key" {
  value     = tls_private_key.this.private_key_pem
  sensitive = true
}

output "storage_path_of_keypair" {
  value = var.save_private_key ? "s3://${aws_s3_object.this.0.bucket}/${aws_s3_object.this.0.key}" : null
}

output "key_name" {
  value = aws_key_pair.this.key_name
}